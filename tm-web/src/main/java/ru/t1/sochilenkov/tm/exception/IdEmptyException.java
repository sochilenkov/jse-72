package ru.t1.sochilenkov.tm.exception;

public final class IdEmptyException extends AbstractException {

    public IdEmptyException() {
        super("Error! Id is empty...");
    }

}
